#ifndef OPENGLWINDOW_CONSOLESTYLE_H
#define OPENGLWINDOW_CONSOLESTYLE_H

#include <dgui/guiStyle.h>
#include <dgui/buttonStyle.h>

struct ConsoleStyle
	: public DGUI::GUIStyle
{
	ConsoleStyle()
		: characterSet(nullptr)
		, outputStyle(nullptr)
		, outputBackground(nullptr)
		, outputBackgroundStyle(nullptr)
		, inputStyle(nullptr)
		, inputBackground(nullptr)
		, inputBackgroundStyle(nullptr)
		, completeListStyle(nullptr)
		, completeListBackground(nullptr)
		, completeListBackgroundStyle(nullptr)
		, completeListButtonStyle(nullptr)
		, completeListButtonBackground(nullptr)
		, completeListButtonBackgroundStyle(nullptr)
		, historySize(15)
		, completeListMaxSize(10)
		, inputOutputPadding(0.0f)
		, maxLines(1024)
		, dumpFile("ConsoleDump.txt")
		, autoexecFile("Autoexec")
		, allowMove(true)
		, allowResize(false)
		, preferLowercaseFunctions(false)
	{}
	~ConsoleStyle() = default;

	DLib::CharacterSet* characterSet;

	std::shared_ptr<DGUI::TextFieldStyle> outputStyle;
	std::shared_ptr<DGUI::GUIBackground> outputBackground; //TODO: Backgrounds shouldn't be shared!
	std::shared_ptr<DGUI::GUIStyle> outputBackgroundStyle;

	std::shared_ptr<DGUI::TextBoxStyle> inputStyle;
	std::shared_ptr<DGUI::GUIBackground> inputBackground;
	std::shared_ptr<DGUI::GUIStyle> inputBackgroundStyle;

	std::shared_ptr<DGUI::ListStyle> completeListStyle;
	std::shared_ptr<DGUI::GUIBackground> completeListBackground;
	std::shared_ptr<DGUI::GUIStyle> completeListBackgroundStyle;

	std::shared_ptr<DGUI::ButtonStyle> completeListButtonStyle;
	std::shared_ptr<DGUI::GUIBackground> completeListButtonBackground;
	std::shared_ptr<DGUI::GUIStyle> completeListButtonBackgroundStyle;

	std::shared_ptr<DGUI::LabelStyle> labelStyle;

	float inputOutputPadding;
	glm::vec2 padding;

	int historySize;
	int completeListMaxSize; //In indexes

	bool allowMove;
	bool allowResize; //Not 100% functional, but still works
	bool preferLowercaseFunctions; //Names default functions "help" instead of "Help"

	unsigned int maxLines; //After this many lines the oldest lines will be removed and written to "dumpFile" instead
	std::string dumpFile;

	std::string autoexecFile;

	std::string labelText;
};

#endif //OPENGLWINDOW_CONSOLESTYLE_H
