#ifndef OPENGLWINDOW_CONSOLE_H
#define OPENGLWINDOW_CONSOLE_H

#include <dgui/guiContainer.h>

#include <dgui/textField.h>
#include <dgui/textBox.h>
#include <dgui/guiManager.h>
#include <dgui/list.h>
#include <dgui/label.h>

#include "consoleStyle.h"
#include "consoleCommand.h"
#include "dictionary.h"
#include "argument.h"

#include <sstream>
#include <unordered_set>

class Console
	: public DGUI::GUIContainer
{
friend class DGUI::GUIManager;
public:
	Console();
	virtual ~Console();

	Console(Console&& other) = delete;
	Console& operator=(Console& other) = delete;

	void Init(DLib::Rect area, const std::shared_ptr<DGUI::GUIStyle>& style, const std::shared_ptr<DGUI::GUIBackground>& background, const std::shared_ptr<DGUI::GUIStyle>& backgroundStyle) override;
	void Update(std::chrono::nanoseconds delta) override;

	void DrawBackground(DLib::SpriteRenderer* spriteRenderer) override;
	void DrawMiddle(DLib::SpriteRenderer* spriteRenderer) override;
	void DrawForeground(DLib::SpriteRenderer* spriteRenderer) override;

	//Command naming rules are:
	//Can only begin with a letter
	//Can only contain letters, numbers, and underscores
	//The console will manage the memory of any command that is successfully (if it returns true) added
	bool AddCommand(ConsoleCommand* command);
	void AddText(const string& text);

	//This method doesn't require () for its parameters; just a space is fine.
	std::string ExecuteFunction(std::string text) /*throws invalid_argument*/;

	void SetPosition(const glm::vec2& newPosition) override;
	void SetPosition(float x, float y) override;
	void SetSize(const glm::vec2& newSize) override;
	void SetSize(float x, float y) override;
	void SetArea(const DLib::Rect& newArea) override;

	void SetDraw(bool draw) override;

	void Activate() override;
	void Deactivate() override;

	bool GetActive() const;

	void Autoexec();
	bool AddAutoexecWatch(const std::string& variable);
	bool RemoveAutoexecWatch(const std::string& variable);
	void PrintAutoexecWatches();

	void OnMouseEnter() override;
	void OnMouseExit() override;
	void OnMouseDown(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
	void OnMouseUp(const DLib::KeyState& keyState, const glm::vec2& mousePosition) override;
	void OnKeyDown(const DLib::KeyState& keyState) override;
	void OnKeyUp(const DLib::KeyState& keyState) override;
	void OnChar(unsigned int keyCode) override;
	void OnScroll(double x, double y) override;

	static DGUI::GUIStyle* GenerateDoomStyle(DLib::ContentManager* contentManager);
	static DGUI::GUIBackground* GenerateDoomStyleBackground(DLib::ContentManager* contentManager);
	static DGUI::GUIStyle* GenerateDoomStyleBackgroundStyle(DLib::ContentManager* contentManager);
	
protected:
	enum COMPLETE_LIST_MODE { HISTORY, COMPLETION };
	COMPLETE_LIST_MODE completeListMode;

	ContextPointers contextPointers;

	//Used for mouse events for input, output, and completeList
	DGUI::GUIManager manager;

	std::shared_ptr<ConsoleStyle> style;

	DGUI::TextField output;
	DGUI::TextBox input;
	DGUI::List completeList;
	DGUI::Label promptLabel;

	//Currently selected index when completeList is in history resp. suggestion mode
	int historyIndex;
	int suggestionIndex;

	glm::vec2 minSize;

	//History of all entered strings
	std::deque<std::string> history;
	//List of created buttons for all buttons of the respective type, ready to move to completeList.
	//When moved, completeList will hold the raw pointers and the unique_ptr will stay inside these vectors
	std::vector<std::unique_ptr<DGUI::GUIContainer>> historyButtons;
	std::vector<std::unique_ptr<DGUI::GUIContainer>> suggestionButtons;
	//All suggestions from the commandDictionary
	std::vector<const DictionaryEntry*> suggestions;

	Dictionary commandDictionary;
	//Maps a string to a ConsoleCommand
	std::map<std::string, std::unique_ptr<ConsoleCommand>> commandMap;

	//<function, line>
	std::unordered_map<std::string, int> autoexecData;
	//<function, arguments>
	std::unordered_map<std::string, std::string> autoexecWatches;

	glm::vec2 grabPosition;
	bool move;

	bool ParseAutoexec();
	bool WriteAutoexec();
	std::vector<Argument> AddAutoexecWatchInternal(const std::vector<Argument>& arguments);
	std::vector<Argument> RemoveAutoexecWatchInternal(const std::vector<Argument>& arguments);
	std::vector<Argument> PrintAutoexecWatchesInternal(const std::vector<Argument>& arguments);

	void UpPressed();
	void DownPressed();
	void EnterPressed();
	void BackspacePressed();
	void DeletePressed();
	void TabPressed();

	//This method requires () for its parameters.
	std::vector<Argument> ExecuteArgumentFunction(std::string text) /*throws invalid argument*/;
	std::vector<std::string> SplitArg(std::string args, char splitBy = ' ');
	std::vector<Argument> EvaluateExpression(std::string expression) /*throws invalid_argument */;

	std::string TrimText(string text);
	std::string Console::TrimTextFrontBack(const string& text);

	template<typename T>
	bool TryParse(std::stringstream& sstream)
	{
		T testVal;
		sstream >> testVal;

		return !sstream.fail();
	}

	//Adds the given text to "history" if needed, otherwise it simply moves it to the top
	void AddToHistoryIfNeeded(const std::string& text);
	void AddToHistory(const std::string& text);
	void MoveToFrontOfHistory(int index);
	void MoveToFrontOfHistory(std::deque<std::string>::iterator iter);

	void MoveHistoryButtonsToCompleteList();
	void MoveSuggestionButtonsToCompleteList();

	void HideCompleteList();
	void ShowCompleteList();
	void UpdateCompleteListPosition(); //Updates the list's size to fit inside window if needed!
	void UpdateCompleteListSize();
	void UpdateCompleteListArea();

	void SwitchCompleteListMode(COMPLETE_LIST_MODE mode);
	void TrimSuggestions(const std::string& text);
	void GenerateSuggestions(const std::string& text);

	void HighlightCompleteListIndex(int index);
	void AcceptText(const std::string& text);

	//Generates the text for which suggestions should be generated
	//"Foo(bar" should generate suggestions for "bar"
	//"Foo(bar,bar1" should generate suggestions for "bar1"
	//And so on
	std::string GenerateSuggestionText();

	bool VerifyCommandName(const string& name);
};

#endif //OPENGLWINDOW_CONSOLE_H
