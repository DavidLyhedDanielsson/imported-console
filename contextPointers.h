#ifndef OPENGLWINDOW_CONTEXTPOINTERS_H
#define OPENGLWINDOW_CONTEXTPOINTERS_H

#include <dgui/textBox.h>
#include <dgui/textField.h>
#include <dgui/list.h>

#include "dictionary.h"

//This is somewhat hackish but it works
class Console;
class ConsoleCommand;

struct ContextPointers
{
	ContextPointers(DGUI::TextBox* input
		, DGUI::TextField* output
		, DGUI::List* completeList
		, Dictionary* commandDictionary
		, std::map<std::string, std::unique_ptr<ConsoleCommand>>* commandMap
		, Console* console)
		: input(input)
		, output(output)
		, completeList(completeList)
		, commandDictionary(commandDictionary)
		, commandMap(commandMap)
		, console(console)
	{}

	DGUI::TextBox* const input;
	DGUI::TextField* const output;
	DGUI::List* const completeList;

	Dictionary* const commandDictionary;
	std::map<std::string, std::unique_ptr<ConsoleCommand>>* const commandMap;

	Console* const console;
};

#endif //OPENGLWINDOW_CONTEXTPOINTERS_H
