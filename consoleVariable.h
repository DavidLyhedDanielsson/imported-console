#ifndef consoleVariable_h__
#define consoleVariable_h__

#include "consoleCommand.h"

class ConsoleVariable : public ConsoleCommand
{
public:
	ConsoleVariable(const std::string& name, bool forceStringArguments)
		: ConsoleCommand(name, forceStringArguments)
	{}
	virtual ~ConsoleVariable() = default;

	virtual std::string GetValue() const = 0;
};

#endif // consoleVariable_h__
